// console.log("Hello World");

// [SECTION] Arithmetic Operators
	// allow us to perform mathematical operations between operands (value on either sides of the operator).
	// it returns a numerical value.

	let x = 100;
	let y = 25;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = x % y;
	console.log("Result of modulo operator: " +remainder);

	// Assignment Operator (=)
		// assigns the value of the "right operand" to a "variable".

	// Basic Assignment Operator
	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// long method
	// assignmentNumber = assignmentNumber + 2;

	// shorthand method
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " +assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " +assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " +assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " +assignmentNumber);

	assignmentNumber %= 2;
	console.log("Result of modulo assignment operator: " +assignmentNumber);

	// PEMDAS (Order of Operations)

	// Multiple Operators and Parenthesis
	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6 // actual

	*/
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS operation: " +mdas);

	// The order of operations can be changed by adding a parenthesis
	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of PEMDAS operation: " +pemdas);

	pemdas = (1 + (2 - 3)) * (4 / 5);
	console.log("Result of PEMDAS operation: " +pemdas);

// [SECTION] Increment and Decrement Operator
	// This operators "add" or "subtract" values by 1 and reassign the value of the variables where the increment/decrement was applied.

	let z = 1;

	// Increment(++)
	// The value of "z" is added by one before storing it in the "increment" variable.
	let increment = ++z;
	// pre-increment both increment and z variable have value of "2"
	console.log("Result of pre-increment: " +increment);
	console.log("Result of pre-increment for z: " +z);

	// The value of "z" is stored in the "increment" variable before it is increased by one.
	increment = z++;
	console.log("Result of post-increment: " +increment);
	console.log("Result of post-increment for z: " +z);

	// Decrement(--)
	z = 5;
	let decrement = --z;
	console.log("Result of pre-decrement: "+decrement);
	console.log("Result of pre-decrement for z: " +z);

	decrement = z--;
	console.log("Result of post-decrement: "+decrement);
	console.log("Result of post-decrement for z: " +z);

// [SECTION] Type Coercion
	// automatic or implicit conversion of value from one data type to another.
	// This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
	// "automatic conversion"

	let numA = '10';
	let numB = 12;

	// Performing addition operation to a number and string variable will result to concatenation.
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	/*
		- The boolean true is also associated with the value of 1.
		- The boolean false is also associated with the value of 0.
	*/
	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

// [SECTION] Comparison Operator
	// are used to evaluate and compare the left and right operands.
	// it return a Boolean value

	let juan = 'juan';

	// Equality Operator (==)
	/*
		-Checks whether the operands are equal/have the same content.
		- Attempts to CONVERT AND COMPARE operands of diffent data types. (type coercion)
	*/

	console.log("Equality Operator: ");
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(false == 0); //true
	console.log('juan' == "juan") //true
	console.log("juan" == "Juan") //false
	console.log(juan == 'juan') //true

	//Equality operator (!=)
	/*
		-checks wetherthe operands are not equal/have different data types.
		-attempts to CONVERT AND COMPARE 
	*/
	console.log("Inequality Operator: ");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(false != 0); //false
	console.log('juan' != "juan") //false
	console.log("juan" != "Juan") //true
	console.log(juan != 'juan') //false

	// Strictly Equaity Operator(===)
	/*
		- check wether the operands are equal/have the same content.
		-also COMPARES the data type of 2 values.
	*/
	console.log("Strictly Equality Operator:");
	console.log(1===1);//true
	console.log(1===2);//false
	console.log(1==='1');//false
	console.log(false===0);//false
	console.log('juan'==="juan");//true
	console.log("juan"==="Juan");//false
	console.log(juan==="juan");//true

	//Strict Inequality Operator (!==)
	/*
		- check wether the operands are not equal/have different content.
		-also COMPARES the data type of 2 values.
	*/
	console.log("Strictly Equality Operator:");
	console.log(1!==1);//false
	console.log(1!==2);//true
	console.log(1!=='1');//true
	console.log(false!==0);//true
	console.log('juan'!=="juan");//false
	console.log("juan"!=="Juan");//true
	console.log(juan!=="juan");//false

//[SECTION] Greater than and less than operator
	// Some comparison operatorscheck wther onevalueis greater or less than to be other value.
	// returns a boolean value.

	let a = 50;
	let b = 65;

	console.log("Greater than and Les Than Operator")
	//GT or Greater Than (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	//LT or Less Than (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	//GTE or GreaterThan or Equal (>=)
	b = 50;
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	//LTE or Less Than or Equal (<=)
	// b = 50;
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr);

	let strNum = "twenty";
	console.log(b > strNum);

//[SECTION] Logical Operators
	// to allowfor a moe specific logical combination of conditions and evaluations.

	// it returns a boolean value.
	let isLegalAge = false;
	let isRegistered = true;

	// Logical And Operator (&& - Double Amprsand)
	//returns TRUE if all operands are true.
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirementsMet)

	// Logical OR Operator (|| - Double pipe)
	//returns TRUE if one of the operands are true.
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsMet)

	// Logical NOT Operator (! - Exclamation point)
	//returns TRUE if one of the operands are true.
	console.log("Result of logical NOT Operator: " + !isRegistered);
	console.log("Result of logical NOT Operator: " + !isLegalAge);